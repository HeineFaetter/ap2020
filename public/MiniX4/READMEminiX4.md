My links still do not work and therefore I have not provided a link for this assignment

For my MiniX4 I have created a program that uses sound. Whenever sound at certain levels are registered, the program will respond by showing 1-3 "sound-arcs" that are part of the standard sound-icon which most of us know.
Above that, the program will reprimand you about your noise level if you reach the higher levels. I chose this because I wanted to make a comment about how we sometimes tend to forget the people that surround us and that we might annoy others with our actions.
Therefore, this program's primary purpose is to remind others of the people that surround them and to be mindful and respectful when surrounding yourself with them.

![screenshot](soundboi.PNG)