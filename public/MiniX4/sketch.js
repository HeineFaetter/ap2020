//mic variable
var mic;
var vol;

function setup() {
  //settting up microphone
  mic = new p5.AudioIn();
  //starting the microphone
  mic.start();
  //canvas
  createCanvas(1600,900);
  angleMode(DEGREES);
  }

function draw() {
  //background
  background(200);
  //logging mouse position
  if(mouseIsPressed){
    console.log(mouseX,mouseY);
  }
  //Registering sound volume
  vol = mic.getLevel();
  //main part of speaker icon
  fill(0);
  beginShape();
  strokeWeight(1);
  vertex(384,380);
  vertex(454,380);
  vertex(594,280);
  vertex(594,630);
  vertex(454,530);
  vertex(384,530);
  endShape(CLOSE);
  //the arcs of the speaker icon - appear at certain volumes
  if(vol>0.5){
    //red arc
    push();
    noFill();
    strokeWeight(25);
    stroke(255,0,0);
    arc(574,455,500,500,315,45);
    pop();
    //text for red arc
    textSize(40);
    text('pls stfu you are annoying everyone else around you', 200,700);
  }
  if (vol>0.2) {
    //yellow arc
    push();
    noFill();
    strokeWeight(25);
    stroke(255,255,0);
    arc(574,455,375,375,315,45);
    pop();
  }
  //text for yellow arc
  if (vol>0.2 && vol<0.5) {
    textSize(40);
    text('keep it down mate', 450,700)
  }
  if (vol>0.015) {
    //green arc
    push();
    noFill();
    strokeWeight(25);
    stroke(0,255,0)
    arc(574,455,250,250,315,45);
    pop();
  }


}
