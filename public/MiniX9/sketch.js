let button;
let i = 0;
let y = 200;
let google;
let cat;
let img;

let url = "https://api.thecatapi.com/v1/images/search?api_key=4bd3cd80-1c41-4a16-8edc-3341e819683c";


function preload(){
  google = loadImage("google.png");
  cat = loadJSON(url,gotData);
}

function setup(){
  createCanvas(400,300);

	button = createButton('search');
	button.position(320, 200);
	button.mousePressed(newRes);
}

function gotData (data) {
  console.log(data)
  cat = data
}


function draw () {

  noLoop();
  background(255);

	//the search box
  noFill();
  rect(100,200,200,20);

	//The text
  fill(0);
  textSize(12);
  text("What is happiness?", 105,215);

  //the indicator
  textSize(15);

  push();
  loop();

  i = i+1;
  if (i % 10 === 0){
    fill(0);
    text("|",210, 216);
  }
  else {
    fill(255);
    text("|",210, 216);
  }
  pop();

//The Google logo
  image(google, 100,70);
	google.resize(0,120);
}

function newRes(){
  img = createImg(cat[0].url);
	img.size(500,500);
	img.position(10,300);
}
