var centre;
var border;
var vel;


function setup() {
  createCanvas(1920,1080);
  centre = createVector(15,32);
  border = createVector(150,0);
	vel = createVector(4,4);
  frameRate(120)
}

function draw() {
  //movement
	var acc = centre.copy();
  acc.sub(border);
  acc.setMag(0.15);
	vel.add(acc);
  border.add(vel);
  background(255,2);

  //throbber
  translate(600,400);
  stroke(0,200,255);
  strokeWeight(10);
  fill(0,200,255)
  ellipse(centre.x,centre.y,25);
  stroke(0,70,255);
  strokeWeight(10)
  ellipse(border.x,border.y,10);
}
