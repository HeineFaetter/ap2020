DISCLAIMER: I simply could not make my RUNME work - I have tried pretty much everything and Noah also ended up giving up on fixing it.

Describe your throbber design, both conceptually and technically.

What is your sketch? What do you want to explore and/or express?
My sketch started out as a an attempt to try to make an atom-shaped throbber. I wanted for an "electron" to move around in an elliptic orbit following the "electron shells" but I could not find out how to do so. Then I came acorss a work with elliptic orbits but the orbit didn't return to the same point every time.
I sat for a while playing with the numbers until I succeeded. I then played around with some of the other factors and ended up with this thing. It doesn't follow one shell but instead goes around in this weird orbit.
I'm not really sure I meant to specifically express anything with this throbber but I wanted to make something a bit more technical and strange.

What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your process of coding)?
Honestly I am not quite sure how the different syntax I have used work exactly but I know that I have used acceleration systax to make it move the way it does. The acc.setMag syntax makes it come close to the center (sort of a gravitational factor) and the velocity vector also must have something to do with its speed (which is raleted to time).
Time is in my understanding relative to the frameRate as the multiple frames are the only thing that creates change in the program and thereby time. In other words, time doesn't exist if the program doesn't change.

Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterise this icon differently?
I throbber is there for our sake, no for the computers. It is there to show us that it is computing and that we need to be patient about it. If it wasn't there we might just make evertything worse by clicking hysterically around to try to force a reaction from the pc but the pc itself doesn't need the throbber to know that it is working.

![link](https://HeineFaetter.gitlab.io/ap2020/MiniX3/)

![screenshot](throbber.PNG)