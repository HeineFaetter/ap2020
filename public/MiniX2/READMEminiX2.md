Describe your program and what you have used and learnt.

I haven't really used that much new syntax. I have however learnt to use the arc() syntax as well as how to find specific coordinates on the screen by clicking the mouse.

How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond? (Try to think through the assigned reading and your coding process, and then expand that to your experience and thoughts - this is a difficult task, you may need to spend sometimes in thinking about it).

Well for my first emoji, I have never seen an albino emoji before and therefore I don't believe they've been represented. I do however find it a bit silly that everyone needs to be represented but that's another talk. 
The fact does remain that I have not yet seen albinos being represented among the other emojis and therefore I decided to make one.
My other emoji is a representation of a videogame character named "Tatchanka" from the game Tom Clancy's Rainbow Six: Siege. I didn't necessarilly make it for the sake of representation but I would actually like for more of these kinds of emojis to appear because gaming has become a huge thing today and I think that more emojis like these would be fun to have.
Therefore I have made this little guy and I actually think it is quite funny.

[link](https://HeineFaetter.gitlab.io/ap2020/MiniX1/)

![screenshot](emojis.PNG)

