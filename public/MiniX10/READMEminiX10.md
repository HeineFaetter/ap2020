**What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?**

The challenges are that in order to communicate a concept to everyone, you have to make it simple enough for everyone to understand. 
Yet at the same time programmes are complex and sometimes a certain level of complexity needs to be explained in order to describe the program properly.
This creates complicated situations where one has to weigh both simplicity and complexity highly and therefore it becomes hard to decide what details should be included and which shouldn't.

**What are the technical challenges for the two ideas and how are you going to address them?**

I think that for the first idea - the game - a challenge is going to be to make "rounds" meaning that we will have to reset the entire canvas and then apply new rules to our next round.
To elaborate we plan to make the first round be a "luring round" meaning that we want to ensure that the player wins the first round in order to lure the player in by giving them a taste of victory.
Afterwards, the successrate falls, meaning that we'll need to reset the entire canvas and then load a new canvas with new successrates implemented. We're not entirely sure how we are going to do this though.

For our second idea, we don't have as much to our idea yet so we're not entirely sure how we're going to make the program. 
I do however think it's going to be hard to implement this ASCII-part into p5.js but maybe it would be possible to do with some sort of API if we could get one.

**What is the value of the individual and the group flowchart that you have produced?**

The group flowchart helps providing some sort of overview as for how we want to proceed with our program.
It also provides an overview for others in order to help them understand the program. 
The individual flowchart helps us see what details we choose to include and which to exclude.

Group flowcharts:

![screenshot](flowchart.PNG)

Individual flowchart:

![screenshot](flowchartown.PNG)