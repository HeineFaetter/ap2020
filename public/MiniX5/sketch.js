var song;

function preload()  {
  song = loadSound('xfilesdub.mp3'); //this song only lasts for around 46 seconds making a reload necessary if you want to replay it
}

function setup() {
  //canvas
  createCanvas(800,600);
  song.play();
}

function draw() {
  //background
  background(0);
  //colors
  let grn=color(0,250,120);
  let blk=color(0)
  let red=color(255,0,0)
  let ylw=color(255,255,0)
  let blu=color(0,0,255)
  let grn2=color(0,200,0)
  let orn=color(255,120,0)
  let ppl=color(120,0,120)

  //stars
  fill(ppl)
  push();
  translate(150, 300);
  rotate(frameCount / -40.0);
  star(0, 0, 30, 70, 5);
  pop();

  fill(grn)
  push();
  translate(90,90);
  rotate(frameCount / -20.0);
  star(0, 0, 30, 70, 5);
  pop();

  fill(red)
  push();
  translate(280,120);
  rotate(frameCount / -30.0);
  star(0, 0, 30, 70, 5);
  pop();

  fill(blu)
  push();
  translate(600,140);
  rotate(frameCount / -10.0);
  star(0, 0, 30, 70, 5);
  pop();

  fill(ylw)
  push();
  translate(620,350);
  rotate(frameCount / -50.0);
  star(0, 0, 30, 70, 5);
  pop();

  fill(orn)
  push();
  translate(720,240);
  rotate(frameCount / -45.0);
  star(0, 0, 30, 70, 5);
  pop();

  //pyramid triangle
  fill(random(255),random(255),random(255))
  strokeWeight(1);
  stroke(grn)
  triangle(400,130,530,400,270,400);

  //pyramid brick lines
  stroke(0)
  line(377,175,423,175)
  line(355,220,445,220)
  line(333,265,374,265)
  line(427,265,467,265)
  line(312,310,332,310)
  line(468,310,488,310)
  line(290,355,509,355)
  line(400,175,400,220)
  line(370,220,370,265)
  line(430,220,430,265)
  line(340,265,340,280)
  line(460,265,460,280)
  line(310,315,310,355)
  line(370,335,370,355)
  line(430,335,430,355)
  line(490,315,490,355)
  line(280,378,280,400)
  line(340,355,340,400)
  line(400,355,400,400)
  line(460,355,460,400)
  line(520,378,520,400)

  //eye
  fill(random(255),random(255),random(255))
  stroke(0)
  ellipse(400,300,140,75);
  fill(blk)
  ellipse(430,300,25,50);

  //lightray
  stroke(255)
  line(0,440,320,295)
  line(0,450,315,305)

  //rainbow
  strokeWeight(0)
  fill(red)
  triangle(435,300,mouseX,mouseY-75,mouseX,mouseY-50)
  fill(orn)
  triangle(435,300,mouseX,mouseY-50,mouseX,mouseY-25)
  fill(ylw)
  triangle(435,300,mouseX,mouseY-25,mouseX,mouseY)
  fill(grn2)
  triangle(435,300,mouseX,mouseY,mouseX,mouseY+25)
  fill(blu)
  triangle(435,300,mouseX,mouseY+25,mouseX,mouseY+50)
  fill(ppl)
  triangle(435,300,mouseX,mouseY+50,mouseX,mouseY+75)

}

function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 2.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
