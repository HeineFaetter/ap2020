DISCLAIMER: As I still am not able to make my links work for some weird reason, I have instead provided a screenrecording of what my program looks like in action.

What is the concept of your work?
I took my first program from the MiniX1 which was a combination of the album cover of Pink FLoyd's Dark Side of the Moon and what is commonly refered to as the illuminati-symbol.
I edited it so that the rainbow follows the mouse, the colors of the pyremid is rapidly and randomly changing, different colored stars turn and a techno-remix of the X-files theme is running.
It has become something very different from the MiniX1 program and is basically a chaos of different colors and music. There's a lot going on.

What is the departure point?
I am not sure what this question means...

What do you want to express?
I am not quite sure myself what I actually want to express with this artwork but I do think that it looks expressive. There's a lot of things going on a lot of things to register both visually and auditively.
The chaotic visuals - both colors and movements, the loud music, the symbolism in the connection between the symbol and the album cover - it's a real mess and it's truly weird.

What have you changed and why?
I already answered the things I changed and as I said, I kinda don't know why I changed the things but I wanted to make a lot of sensual output. Staring at the program for too long is almost annoying and it sort of becomes too much.


Reflect upon what Aesthetic Programming might be (Based on the readings that you have done before (especially this week on Aesthetic Programming as well as the class01 on Why Program? and Coding Literacy), What does it mean by programming as a practice, or even as a method for design? Can you draw and link some of the concepts in the text and expand with your critical take? What is the relation between programming and digital culture?)
Well this is a long question. Aesthetic programming seems to be more than just learning syntax and how to use it. It invites us to dig deeper and ask ourselves questions about programming. It makes us examine the aspects of programming that connects to for example our societies, politics, ethics etc.
I makes us reflect upon programming in new ways while still teaching us how to program which is equally important which all together grants us a wider understanding of programming and digital culture as well.

![screenshot](remadedarkside.PNG)