var t = 0;
var speed = 0.05;
var x = 0;

function setup() {
  createCanvas(400, 400);
  background(220);
  frameRate(120);

}

function draw() {

  //middle figure
  noStroke();
  fill(random(255),random(255),random(255));
	t+= speed;
  push();
  translate (width/2, height/2);
  rotate(t);
	triangle(t, -17.32, t+15, 8.86, t-15, 8.86);
  pop();

//change of direction
  if (t > 102) {
    speed = -0.05
  } else if (t < -102) {
    speed = 0.05
  }

  //horizontal figures (along the x-axis) follows the middle figure direction
  for (var xpos = 50; xpos < width; xpos += 50) {
    x += speed/12;
    push();
    translate (xpos, 50);
    rotate(x);
    triangle(0, -17.32, 15, 8.86, -15, 8.86);
    pop();
    push();
    translate (xpos, 350);
    rotate(x);
    triangle(0, -17.32, 15, 8.86, -15, 8.86);
    pop();
  }

  //vertical figures (along the y-axis) follows the middle figure direction
  for (var ypos = 50; ypos < height; ypos += 50) {
    x += speed/12;
    push();
    translate (50, ypos);
    rotate(x);
    triangle(0, -17.32, 15, 8.86, -15, 8.86);
    pop();
    push();
    translate (350, ypos);
    rotate(x);
    triangle(0, -17.32, 15, 8.86, -15, 8.86);
    pop();
  }
}
