*DISCLAIMER: My links still don't work and I have therefore not included one in my README.*

**What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors?**

In the middle a triangle is spinning around itself while also spiralling outwards and inwards consistently and changing direction. Its colors change randomly as well and it leaves a trail of randomly colored triangles.
Around it in a perfect square are other spinning triangles whose colors also change. All of this together creates a very random while still symetric and harmonic image.

**What's the role of rules and processes in your work?**

I think I'd say that the role of my rules is to make sort of a random image yet still harmonic and controlled. To make images taht are never the same yet still very much alike because of the randomness.
Making thousands of distinct yet unrecognizable pictures because they are all equally random. All the same yet different.

**Draw upon the assigned reading(s), how does this mini-exericse help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?**

I realized while making this that my work actually relates a lot to the "pseudorandomness" which we read about in one of our texts. 
Pseudorandomness described that everything that was randomized by a computer was only partially random as the randomness was created by an algorithm and therefore based on something predictable and logical.
The same can be said for my work. Despite every outcome being different they are always based on logic and order. Randomness based on order. Harmony based on chaos.

![screenshot](autogenerator.PNG)