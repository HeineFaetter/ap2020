DISCLAIMER: My links are still unable to function for some unknown reason and I have therefore not included a RUNME in my assignment.

Describe how does your game/game objects work?

I had a lot of trouble making this program and it has not finished as I intended it to work. My intentions were inspired from the "BLOONS" games where you have to pop a lot of baloons with darts.
I wanted to make a number of baloons which when pressed with the mouse would disappear as if they had popped. A counter would then count down the number of ballons left and when it reached zero it should then display some sort of succes message.
The balloons would above that move around making it harder to hit them with the mouse and I would've also liked to include a pop-sound. I was however unable to figure out how to use the splice-syntax properly and I could therefore not finish my program as I intended.
I would also have liked to turn the mouse into some sort of reticule/crosshair resembling a holosight used on guns but I could not figure out how to do this properly either.

Describe how you program the objects and their related attributes and methods in your game.
I defined my balloons' attributes via the class function. Here I gave it a randomized spawn point and a set radius (as I used an ellipse to represent the balloons).
I also added a string on the bottom of the balloons and I made it appear more transparent to make it look more like a balloon. I made a move and a show funcction in order to both show the balloons and make them move around.

What are the characteristics of object-oriented programming and the wider implications of abstraction?
Object oriented programming is a way to enter the deeper layers of a program and reach a higher point of abstraction, meaning that we view our objects differently in order to achieve a higher level of detail and sometimes also realism. It makes us draw things and concepts from our own world and turn them into code.
It also lets the programmer decide what the defining characteristics of things are and which are irrelevant.

Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?
I'm not entirely sure how to answer this question but I guess I'll give it a shot. I'd like to pick the BLOONS games that inspired my project. In these games they for example make use of gravity when throwing the darts that are to pop the balloons.
This is a higher level of abstraction than my own game and it also makes for a very different experience. There also other kinds of abstractions in the games. For example, the darts are thrown by a monkey in the games which makes it more unrealistic yet still being on a higher level of abstraction.
In this way there are more ways to achieve abstraction in games and look at your programming in an object-oriented way.

![screenshot](balloons.PNG)