let balloons = [];

function setup() {
  createCanvas(1600,900);
  for (let i = 0; i< 10; i++)
    balloons.push(new Balloon());

}

function mousePressed() {
  for (let i = 0; i < balloons.length; i++) {
    balloons[i].popped(mouseX,mouseY);
  }
}

function draw () {
  background(50, 89, 100);

  for (let i = 0; i < balloons.length; i++) {
    balloons[i].move();
    balloons[i].show();
  }
}

class Balloon {
  constructor(x,y,r) {
    this.x = random(400,1200);;
    this.y = random(200,700);
    this.r = 30;
  }

  popped(px,py) {
    let d = dist(px,py,this.x,this.y);
    if (d < this.r) {
      balloons.splice(i,1)
    }
  }
  move() {
    this.x = this.x + random(-5,5);
    this.y = this.y + random(-5,5);
  }
  show() {
    stroke(255);
    strokeWeight(4);
    fill(255,0,0,70);
    ellipse(this.x,this.y,this.r*2);
    line(this.x,this.y+30,this.x,this.y+90);
  }
}
