How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
I would say that it is firsty a lot of fun when what you do works. I like that I can create just about whatever I feel like as long as I am able to understand the system of commands that are required to do so.  I did however have to do a lot of calculation in order to place different objects in the right place which got a bit tiresome.

How is the coding process different from, or similar to, reading and writing text?
It is different from reading in the way that it is not whole sentences - just words and numbers - and I have to make sense out of these things that would otherwise be rubbish.

What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?
That is actually hard to answer at this point. I think I am only beginning to understand what programming and code truly is and therefore I doesn't really have a lot of meaning to be at the moment. The readings, however, grant me new perspectives on code and what it actually is and how we can use it.

![screenshot](Dark side of the Illuminati.PNG)
[link](https://HeineFaetter.gitlab.io/ap2020/public/MiniX1/)