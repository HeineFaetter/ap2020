function setup() {
  createCanvas(800,600);
}
function draw() {
  background(0);
  let grn=color(0,250,120);
  let blk=color(0)
  let red=color(255,0,0)
  let ylw=color(255,255,0)
  let blu=color(0,0,255)
  let grn2=color(0,200,0)
  let orn=color(255,120,0)
  let ppl=color(120,0,120)
  fill(grn)
  strokeWeight(1);
  stroke(grn)
  triangle(400,130,530,400,270,400);
  fill(grn)
  stroke(0)
  ellipse(400,300,140,75);
  fill(blk)
  ellipse(430,300,25,50);

  line(377,175,423,175)
  line(355,220,445,220)
  line(333,265,374,265)
  line(427,265,467,265)
  line(312,310,332,310)
  line(290,355,509,355)

  line(400,175,400,220)
  line(370,220,370,265)
  line(430,220,430,265)
  line(340,265,340,280)
  line(460,265,460,280)
  line(310,315,310,355)
  line(370,335,370,355)
  line(430,335,430,355)
  line(490,327,490,355)
  line(280,378,280,400)
  line(340,355,340,400)
  line(400,355,400,400)
  line(460,355,460,400)
  line(520,378,520,400)
  stroke(255)
  line(0,440,320,295)
  line(0,450,315,305)
  strokeWeight(0)
  fill(red)
  triangle(435,300,800,330,800,355)
  fill(orn)
  triangle(435,300,800,355,800,380)
  fill(ylw)
  triangle(435,300,800,380,800,405)
  fill(grn2)
  triangle(435,300,800,405,800,430)
  fill(blu)
  triangle(435,300,800,430,800,455)
  fill(ppl)
  triangle(435,300,800,455,800,480)

}
