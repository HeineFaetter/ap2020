**DISCLAIMER: My links still do not work. I have tried to get help from Noah and to add the libraries-folder to ALL of the folders related to my MiniX but somehow it is still unable to find the file.**

**Provide a title of your work and a short description of the work (within 1000 characters).**

This work has been made in collaboration with Mads Lindgaard. We have chosen to call it "Encyclopedia". 
The work it essentially a listing of random categories and things that fit under those categories.
An encyclopedia of random and perhaps also pretty useless facts and terms. 
For example it contains a list of apple sorts and another list of different kinds of clouds. 
It does not contain further explanations about the different things included and it does not provide an explanation of how the things differ from each other with their categories.
Just listings of random stuff.

**Describe your program in terms of how it works, and what you have used and learnt?**
Our program really isn't very complicated in terms of how it works.
All of our listings are lined up in a JSON-file - each category being an array within the file. 
This file is then preloaded into the sketch-file where we use two for-loops to display all of the categories and their respective subjects htorugh their indecies in their arrays.
One is then able to scroll down and read all of the different things that are listed.


**Analyze and articulate your work:**

**Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language) in relation to code and language.**
**How would you reflect upon various layers of voices and/or the performativity of code in your program?**


The code for this work is in itself not really worth looking at, as there is not really that much code to look at. 
It's not some sort of poem written into the code and there are no redundant functions, variables or other syntax that are added in order to make a message.
The lingual aspect of our code is very minimal.
There aren't any extra layers of voices to complement the work - no sounds of any kind are included either.
That does not mean that the work itself doesn't relate to language in any way. Language is not just for communication - it is also a tools for us to help archive and contain knowledge.
This work reflects an archive of words and categories that we use in order for us to make sense of information and to separate this information so that we know how to differ between things.
Language helps us defining what things are and how things are different.
In the bottom of the work we have also written "MORE TO COME - FEEL FREE TO ADD MORE". 
This reflects another part of language - it keeps expanding. 
Language is not consistently the same but on the contrary an evolving matter.
Everytime something new is invented we need to categorize it in order to reach a common understanding of what it is.
Sometimes it also goes the other way around and words or expressions die.
Languages are constantly evolving - sometimes even indepently of each other. 

RUNME: [link](https://HeineFaetter.gitlab.io/ap2020/MIniX8/)

![screenshot](encyclopedia.PNG)
